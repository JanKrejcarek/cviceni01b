const axios = require('axios');

exports.handler = async function(event, context) {
    const base = `https://www.mapakriminality.cz/api/crimes`
    
    const options = {
        method: 'get',
        url: base,
        params: event.queryStringParameters
    }
    console.log(JSON.stringify(options));
    
    try {
        const response = await axios(options); 

        console.log("Data retrieved");
        return {
            statusCode: 200,
            body: JSON.stringify(response.data)
        };          
    } catch (error) {        
        console.log("Error retrieving data");
        return {
            statusCode: error.response.status,
            body: JSON.stringify({message: 'Error retrieving data'})
        };          
    }
}